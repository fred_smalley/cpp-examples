/*!
 * @file  hello.h
 * @author  Fred Smalley
 * @date 4-19-16
 */

/*!
 * This is the header guard and it makes sure this header is
 * not included more than once during compilation time
 */
#ifndef hello_hello_H     // if we haven't defined a header include flag yet do everything until endif
#define hello_hello_H     // define the header include flag

#include <iostream>       // include the header for iostream which holds the output function std::cout and std::endl
#include <string>         // include the header for the string class

// define function prototypes so compiler won't complain about when we define our functions
void stringLiteral();
void individualCharacters();
void cstring();
void stringObject();

#endif    // end the if not defined clause
