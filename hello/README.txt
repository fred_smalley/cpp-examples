Hello Example

This example will demonstrate:
- header files
- header guards
- preprocessor commands
- function prototypes
- void function calls
- std::cout and std::endl output functions
- string literal creation
- cstring instantiation
- direct cstring manipulation and null character
- string object creation

This example is documented and should be easy to understand.

To compile with g++ run the batch file 'compile'
