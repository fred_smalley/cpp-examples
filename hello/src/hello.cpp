/*!
 * @file  hello.cpp
 * @author  Fred Smalley
 * @date  4-19-16
 */

#include "hello.h"

/*!
 * @function main
 * This is the obligatory main method which is the first method
 * the program will run.  This is where the program starts and
 * this is where everything else will be called from. If a method
 * or function is defined elsewhere but not called from this method
 * or from any sub methods (or any sub sub methods etc.) then it
 * will be compiled but not executed.
 */
int main () {
  // call different methods
  stringLiteral();
  individualCharacters();
  cstring();
  stringObject();

  // return zero as we have finished
  return 0;
}

/*!
 * @function stringLiteral
 * This method will print a string literal to the screen
 */
void stringLiteral() {
  // print the type
  std::cout << "STRING LITERAL -> ";

  // print the string literal
  std::cout << "hello" << std::endl << std::endl;
}

/*!
 * @function individualCharacters
 * This method will declare and initialize four character variables
 * then print them out to the screen
 */
void individualCharacters() {
  // print the type
  std::cout << "INDIVIDUAL CHARACTERS -> ";

  // make character variables
  char h_character = 'h';
  char e_character = 'e';
  char l_character = 'l';
  char o_character = 'o';

  // print the characters
  std::cout << h_character
    << e_character
    << l_character
    << l_character
    << o_character
    << std::endl << std::endl;
}

/*!
 * @function cstring
 * This method will instantiate a cstring and fill it with characters
 * then print it to the screen
 */
void cstring() {
  // print the type
  std::cout << "CSTRING -> ";

  // instantiate the cstring with correct size (number of letters + 1)
  char hello_cstring[6];

  // fill cstring with letters
  hello_cstring[0] = 'h';
  hello_cstring[1] = 'e';
  hello_cstring[2] = 'l';
  hello_cstring[3] = 'l';
  hello_cstring[4] = 'o';

  // put end character at the end
  hello_cstring[5] = '\0';

  // print the cstring
  std::cout << hello_cstring << std::endl << std::endl;
}

/*!
 * @function stringObject
 * This method will create a string object and print it to the screen
 */
void stringObject() {
  // print the type
  std::cout << "STRING OBJECT -> ";

  // create the string object
  std::string hello_string = "hello";

  // print the string object to the screen
  std::cout << hello_string << std::endl << std::endl;
}
